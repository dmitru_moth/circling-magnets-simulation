#include <allegro5/allegro.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_IMPLEMENTATION
#define NK_ALLEGRO5_IMPLEMENTATION
#include "nuklear.h"
#include "nuklear_allegro5.h"

#define DISPLAY_WIDTH 1280
#define DISPLAY_HEIGHT 720

struct state
{
  float I; // сила тока, А
  float B; // индукция, Тл
  float l; // длина батарейки, м
  float m; // масса, кг
  float x; // позиция
  float a; // ускорение, м / с^2
  float r1;
  float r2;
};

int
main (int argc, char *argv[])
{
  struct state s;
  float wait = 0.f;
  for (int i = 0; i < argc; i++)
    {
      if (i == argc - 1)
        break;

      if (strcmp (argv[i], "-I") == 0)
        s.I = atof (argv[i + 1]);
      else if (strcmp (argv[i], "-B") == 0)
        s.B = atof (argv[i + 1]);
      else if (strcmp (argv[i], "-l") == 0)
        s.l = atof (argv[i + 1]);
      else if (strcmp (argv[i], "-m") == 0)
        s.m = atof (argv[i + 1]);
      else if (strcmp (argv[i], "-r1") == 0)
        s.r1 = atof (argv[i + 1]);
      else if (strcmp (argv[i], "-r2") == 0)
        s.r2 = atof (argv[i + 1]);
      else if (strcmp (argv[i], "--wait") == 0)
        wait = atof (argv[i + 1]);
    }

  if (!al_init ())
    {
      printf ("Failed to initialize allegro5!\n");
      return 1;
    }

  if (!al_install_keyboard ())
    {
      printf ("Failed to install keyboard!\n");
      return 1;
    }

  if (!al_install_mouse ())
    {
      printf ("Failed to install mouse!\n");
      return 1;
    }

  al_set_mouse_wheel_precision (150);

  al_set_new_display_flags (ALLEGRO_OPENGL | ALLEGRO_WINDOWED
                            | ALLEGRO_RESIZABLE);
  al_set_new_display_option (ALLEGRO_VSYNC, 1, ALLEGRO_SUGGEST);
  al_set_new_window_title ("simulation");

  ALLEGRO_DISPLAY *display = al_create_display (DISPLAY_WIDTH, DISPLAY_HEIGHT);
  if (!display)
    {
      printf ("Failed to create display!\n");
      return 1;
    }

  ALLEGRO_EVENT_QUEUE *queue = al_create_event_queue ();
  if (!queue)
    {
      printf ("Failed to create event queue!\n");
      return 1;
    }

  al_register_event_source (queue, al_get_keyboard_event_source ());
  al_register_event_source (queue, al_get_mouse_event_source ());
  al_register_event_source (queue, al_get_display_event_source (display));

  NkAllegro5Font *font
      = nk_allegro5_font_create_from_file ("DejaVuSans.ttf", 12, 0);
  struct nk_context *nk_ctx
      = nk_allegro5_init (font, display, DISPLAY_WIDTH, DISPLAY_HEIGHT);

  sleep (wait);

  bool is_running = true;
  float previous_time = al_get_time ();
  float previous_x = s.x;

  while (is_running)
    {
      ALLEGRO_EVENT event;
      nk_input_begin (nk_ctx);
      while (al_get_next_event (queue, &event))
        {
          switch (event.type)
            {
            case ALLEGRO_EVENT_DISPLAY_CLOSE:
              is_running = false;
              break;
            }
          nk_allegro5_handle_event (&event);
        }
      nk_input_end (nk_ctx);

      al_clear_to_color (al_map_rgb (0, 0, 0));

      float current_time = al_get_time ();
      float delta_time = current_time - previous_time;
      previous_time = current_time;

      if (nk_begin (nk_ctx, "Settings",
                    nk_rect (al_get_display_width (display) - 200, 0, 200,
                             al_get_display_height (display)),
                    NK_WINDOW_TITLE | NK_WINDOW_SCALABLE | NK_WINDOW_MOVABLE))
        {
          nk_layout_row_dynamic (nk_ctx, 30.f, 1);

          nk_value_float (nk_ctx, "delta time: ", delta_time);

          nk_value_float (nk_ctx, "I", s.I);
          nk_value_float (nk_ctx, "B", s.B);
          nk_value_float (nk_ctx, "l", s.l);
          nk_value_float (nk_ctx, "m", s.m);
          nk_value_float (nk_ctx, "x", s.x);
          nk_value_float (nk_ctx, "a", s.a);

          if (s.r1 == 0 || s.r2 == 0)
            {
              s.x += (s.I * s.B * s.l * pow (delta_time, 2)) / s.m;
              s.a = (s.x - previous_x) / delta_time;
              previous_x = s.x;
            }
          else
            {
	      
            }
        }

      nk_end (nk_ctx);

      al_draw_filled_circle (s.x * 100, al_get_display_height (display) / 2.f,
                             12.f, al_map_rgb (0, 255, 0));

      nk_allegro5_render ();
      al_flip_display ();
    }

  nk_allegro5_font_del (font);
  nk_allegro5_shutdown ();
  al_destroy_event_queue (queue);
  al_destroy_display (display);

  return 0;
}
