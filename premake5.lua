workspace "simulation"
  configurations { "Debug", "Release" }

project "simulation"
  kind "ConsoleApp"
  language "C"
  targetdir "bin/%{cfg.buildcfg}"
  objdir "bin/obj/%{cfg.buildcfg}"

  files { "main.c" }
  links { "m", "allegro", "allegro_image", "allegro_ttf", "allegro_font", "allegro_primitives" }
  
  filter "configurations:Debug"
    defines { "DEBUG" }
    symbols "On"

  filter "configurations:Release"
    defines { "NDEBUG" }
    optimize "On"
